import {Client} from 'discord.js'
import dotenv from 'dotenv'
import {XMLParser} from 'fast-xml-parser'
import fs from 'fs'
import he from 'he'
import {CronJob} from 'cron';

dotenv.config()
const client = new Client({intents: ['Guilds', 'GuildMessages', 'DirectMessages']})

client.once('ready', () => {
  console.log('Discord bot is ready! 🤖')
})

await client.login(process.env.DISCORD_TOKEN)

const fetchData = async () => {
  try {
    const response = await fetch('https://ircc.canada.ca/francais/travailler/eic/selection.xml')
    
    if (!response.ok) throw new Error('Network response was not ok')
    
    const xml = await response.text()
    const json = parsingXml(xml)
    
    sendMessage(json)
    
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

const parsingXml = (xml: string): { date: any; data: any } => {
  const options = {
    attributeNamePrefix: '',
    ignoreAttributes: false,
    parseAttributeValue: true,
  }
  const parser = new XMLParser(options)
  const data = parser.parse(xml)
  
  return {
    "date": he.decode(data.temp.chancesdate),
    "data": data.temp.country.filter(item => item.location === 'France' && item.category === 'wh')[0]
  }
}

const sendMessage = ({date, data}) => {
  fs.readFile('out/last-check.txt', 'utf8', (err, content) => {
    if (err) {
      console.error('Erreur de lecture du fichier :', err);
      return;
    }
    
    if (content === date) return
    
    const channel: any = client.channels.cache.get(process.env.DISCORD_CHANNEL_ID);
    channel.send(`**${date}**\n\nQuota : ${data.quota}\nPremière ronde d’invitations : ${data.first}\nRonde finale d’invitations : ${data.second}\nInvitations envoyées à ce jour : ${data.invitations}\nCandidats inscrits dans le bassin : ${data.candidates}\nPlaces disponibles : ${data.spots}\nChances : ${data.chances}`)
    
    fs.writeFile('out/last-check.txt', date, err => {
      if (err) {
        channel.send(err);
        console.error(err);
      }
    });
  });
}

const cron = new CronJob(
  '*/30 * * * *',
  fetchData,
  null,
  true,
  'Europe/Paris'
)
